# Rewriting semantics for STCTL

We give a rewriting logic semantics for Strategic Timed CTL (STCTL),
interpreted over Parametric Timed Automata networks with asynchronous
execution. We use rewriting modulo SMT to capture clock constraints and timed
parameters as terms, and specify the continuous semantics of STCTL via
rewriting strategies. 

## Getting started

The project was tested in [Maude 3.4](http://maude.cs.illinois.edu/). No
extra libraries or files are needed. 

## Maude files

See [here](./maude/README.md) for further information about the specification. 

## Case Studies 

The case studies can be found in the folder `maude/examples` and it includes:

- [_coffee.maude_](./maude/examples/coffee.maude): coffee machine extended with
  parameters [1].
- [_train-controller.maude_](./maude/examples/train-controller.maude): train
  controller introduced in [2].
- [_timed-dinning-phil.maude_](./maude/examples/timed-dinning-phil.maude):
  well-known dining philosopher problem extended with clocks [3].
- [_timed-dinning-phil-spec.maude_](./maude/examples/timed-dinning-phil-spec.maude):
  synthesizing a controller from a universal formula.
- [_voting.maude_](./maude/examples/voting.maude): voting system described in [4]. The file
  [voting.csv](./maude/examples/voting.csv) contains the results of benchmarking Maude with the
  implementation of this system in Imitator [4]. 


Further information about the case studies is available 
[here](./maude/examples/README.md).

### Benchmarks for Parameter Synthesis

The directory [EF-benchmarks](./EF-benchmarks) contains different PTA models
for testing parameter synthesis properties. The benchmarks were taken from the
[Imitator](https://www.imitator.fr/benchmarks.html) library. 


See the headers of each file for further information and [this paper](./paper.pdf).

## References

[1] Étienne André. 2019. What’s decidable about parametric timed automata?
Int. J. Softw. Tools Technol. Transf. 21, 2 (2019), 203–219.
https://doi.org/10.1007/s10009-017-0467-0

[2] Étienne André, Michal Knapik, Wojciech Penczek, and Laure Petrucci. 2016.
Controlling Actions and Time in Parametric Timed Automata. In 16th International
Conference on Application of Concurrency to System Design, ACSD 2016, Torun,
Poland, June 19-24, 2016, Jörg Desel and Alex Yakovlev (Eds.). IEEE Computer
Society, 45–54. https://doi.org/10.1109/ACSD.2016.20

[3] Magdalena Kacprzak, Artur Niewiadomski, Wojciech Penczek, and Andrzej
Zbrzezny. 2023. SMT-Based Satisfiability Checking of Strategic Metric Temporal
Logic. In Frontiers in Artificial Intelligence and Applications, Kobi Gal, Ann
Nowé, Grzegorz J. Nalepa, Roy Fairstein, and Roxana Rădulescu (Eds.). IOS Press.
https://doi.org/10.3233/FAIA230394

[4] Jaime Arias, Wojciech Jamroga, Wojciech Penczek, Laure Petrucci, Teofil Sidoruk:
Strategic (Timed) Computation Tree Logic. AAMAS 2023: 382-390

