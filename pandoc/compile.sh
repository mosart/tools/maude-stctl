#!/bin/bash

BASE_PATH=".."
OUTPUT_FOLDER="maude-stctl"
IMAGES_FOLDER="$BASE_PATH/maude/examples"

# 1) generate the html files
find $BASE_PATH -type f -name '*.md' -print0 | xargs -0 -n2 -P3 -I{} \
  pandoc \
  --self-contained \
  --shift-heading-level-by=-1 \
  --mathml \
  --from markdown \
  --to html \
  --resource-path . \
  --resource-path $IMAGES_FOLDER \
  --template template.html \
  --lua-filter ./replace.lua \
  -o {}.html \
  {}

# 2) rename the files README.md.html into index.html
for f in $(find $BASE_PATH -name '*.md.html'); do mv -- "${f}" "${f%README.md.html}index.html"; done

# 3) create zip file
rm -rf "$OUTPUT_FOLDER.zip"

mkdir -p "$OUTPUT_FOLDER/maude/examples"
cp -r $BASE_PATH/index.html $OUTPUT_FOLDER
cp -r $BASE_PATH/maude/*.maude $OUTPUT_FOLDER/maude
cp -r $BASE_PATH/maude/examples/{index.html,*.maude} $OUTPUT_FOLDER/maude/examples

zip -rq "$OUTPUT_FOLDER.zip" $OUTPUT_FOLDER/*
rm -rf $OUTPUT_FOLDER

echo -e "\nFile ${OUTPUT_FOLDER}.zip was successfully generated :D"
