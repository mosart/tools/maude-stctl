new_links = function(el)
  el.target = string.gsub(el.target, "README.md", 'index.html');
  return el
end

return {
  {Link  = new_links},
}