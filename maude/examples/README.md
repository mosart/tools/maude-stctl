# Experiments

In the following, we present the case studies used in our experiments.

## Coffee Machine

This is the well-known coffee machine example extended with parameters. It
was introduced in [1] and consists of 4 locations, 2 clocks ($x_1$ and $x_2$),
and 3 parameters ($p_1$, $p_2$, and $p_3$). Both clocks are parametric clocks
and no diagonal constraints are used. The machine starts in the `idle` state,
and it can stay there for an arbitrary long time. Then, whenever the user
presses the button `bStart`, the system enters location `add_sugar`, resetting
both clocks. The machine can remain in this location as long as the invariant
($x_2 \leq p_2$) is satisfied; there, the user can add a dose of sugar by
pressing the button `bSugar`, provided the guard ($x_1 \geq p_1$) is satisfied,
which resets $x_1$. That is, the user cannot press twice the button (and hence
add two doses of sugar) in time less than $p_1$. Then, $p_2$ time units after
the machine left the idle mode, a cup is delivered (action `cup`), and the
coffee is being prepared; eventually, $p_3$ time units after the machine left
the idle mode, the coffee (action `coffee`) is delivered. Then, after 10 time
units, the machine returns to the idle mode -- unless a user again requests a
coffee by pressing the button `bStart`.

<figure style="text-align: center;">
  <img src="./images/coffee.png" width="500"/>
  <figcaption style="font-weight: bold;">Coffee Machine Automaton</figcaption>
</figure>

The property $\mathtt{\exists\mathsf{F}~coffee@done}$ holds:

```
Maude> srew wrap(init, EF (coffee @ done)) using check .
srewrite in COFFEE : wrap(init, EF coffee @ done) using check .

Solution 1
rewrites: 6254 in 13ms cpu (11ms real) (471217 rewrites/second)
result State: Yes(empty, rr(var(p2)) + (-1).NzInt * rr(var(p3)) <= (0).Zero and (-1).NzInt * rr(var(p1)) <= (0).Zero
    and (-1).NzInt * rr(var(p2)) <= (0).Zero and (-1).NzInt * rr(var(p3)) <= (0).Zero)
```

The resulting constraint says that, location $done$ is reachable if
$p_2 \leq p_3$.

Maude finds
two possible strategies when checking the formula
$\mathtt{\langle\langle coffee \rangle\rangle ~coffee@done}$,
both of them moving from the location `addsugar` to `preparing_coffee` thus
avoiding the loop adding sugar ad infinitum:

```
Maude> srew wrap(init, << coffee >> (AG (EF (coffee @ done)))) using check .
srewrite in COFFEE : wrap(init, << coffee >> AG EF coffee @ done) using check .

Solution 1
rewrites: 35867 in 23ms cpu (22ms real) (1543729 rewrites/second)
result State: Yes((coffee : bstart : idle -> addsugar, coffee : bstart : done -> addsugar, coffee : cup : addsugar ->
    preparing, coffee : coffee : preparing -> done), (-1).NzInt * rr(var(p1)) <= (0).Zero and (-1).NzInt * rr(var(p2))
    <= (0).Zero and (-1).NzInt * rr(var(p3)) <= (0).Zero)

Solution 2
rewrites: 35919 in 23ms cpu (22ms real) (1545967 rewrites/second)
result State: Yes((coffee : bstart : idle -> addsugar, coffee : cup : addsugar -> preparing, coffee : coffee :
    preparing -> done, coffee : sleep : done -> idle), (-1).NzInt * rr(var(p1)) <= (0).Zero and (-1).NzInt * rr(var(
    p2)) <= (0).Zero and (-1).NzInt * rr(var(p3)) <= (0).Zero)

No more solutions.
rewrites: 35919 in 23ms cpu (22ms real) (1545967 rewrites/second)
```

The two solution differ about the strategy at `done`, either going to the
location `idle` or going directly to `addsugar`.

## Train Controller

This model is introduced in [2] and it comprises two components : the _train_
and the _gate controller_. A sensor is located far from the gate, which sends a
signal to inform the controller that the train is approaching (so that the
controller start lowering the gate). Another sensor is located close to inform
the gate that the train is very near, triggering an emergency lowering if the
gate is not yet closed.

An intruder can sabotage the system by either disabling the far sensor, thus
preventing the detection of an approaching train, or cut the power supply in the
gate house, hence preventing the gate to lower. Moreover, the gate has a
security mechanism that automatically lowers the gate some time after the power
has been shut off, and another security mechanism with an emergency gate
lowering when the train is very near.

The system is modelled by three automata that model the train, the gate and the
intruder. They comprise parameters (in brown), clocks (in blue), actions (in
green), and discrete values (in purple).

<figure style="text-align: center;">
  <img src="./images/train.png" width="500"/>
  <figcaption style="font-weight: bold;">Train Automaton</figcaption>
</figure>

Initially, the train is far, and remains far until it has travelled for `p_far`
time. At that date, it becomes approaching. Note that there are two similar
transitions form location `far` to location `approaching`. One considers the
sensor is active and thus triggers the sensor `sensor_far` action, while the
other caters for the inactive sensor case (disabled by the intruder). In both
cases, the train clock is reset to count time elapsed from then on. When the
train becomes `very_near`, the automaton changes location again, by triggering
`sensor_close`. Note that here there is only one transition as the intruder
cannot tamper with the close sensor. Then the last part of the train approach
takes place, and when it reaches the gate, it results in two different states
according to the status of the gate. If `gate_down` is false, there is a
`crash`, otherwise the train resumes its journey.

<figure style="text-align: center;">
  <img src="./images/intruder.png" width="500"/>
  <figcaption style="font-weight: bold;">Intruder automaton</figcaption>
</figure>

The intruder starts in an urgent location where (s)he has to choose immediately
between the two sabotage possibilities. Although the actions are different
(`break_sensor`, and `power_cut`), they both deactivate the sensors as the
communication between sensors and gate is broken.

<figure style="text-align: center;">
  <img src="./images/gate.png" width="500"/>
  <figcaption style="font-weight: bold;">Gate Automaton</figcaption>
</figure>

Initially, the gate starts with the barrier `up`, and can change on reception of
a `sensor_far` or a `sensor_close` signal or detecting a `power_cut`, leading to
locations `waiting`, `emergency_lowering` or `no_power`, respectively. In all
cases, the clock starts ticking. After some time, the gate starts `lowering`
until it is `down`. The `power_cut` can also happen anytime, leading to the
`no_power` location. After a power shutdown, the emergency procedure states that
if the power is not back an `emergency_lowering` takes place. It is also the
case if the `sensor_close` is triggered while the train should still be
approaching. Finally, when the train has passed the crossing, the barrier gets
back `up`.

We have synthesized the strategy for the formula
$\langle\langle intruder \rangle\rangle\, \exists \mathsf{F}\, train@crash$.
Maude finds in less than one second, all the possible parameters valuations
reported in [2]. The resulting strategies determine the way to perform the
attack either by breaking a sensor or cutting the power:

```
Maude> srew wrap(init, << intruder >> EF (train @ crash)) using check .

srewrite in TRAIN : wrap(init, << intruder >> EF train @ crash) using check .

Solution 1
rewrites: 596936 in 292ms cpu (292ms real) (2038632 rewrites/second)
result State: Yes((intruder : choosesensor : choosing -> walkingsensor, intruder : breaksensor : walkingsensor ->
    intruderdone), (1).NzNat + (-1).NzInt * rr(var(pverynear)) <= (0).Zero and (1).NzNat + (-1).NzInt * rr(var(
    pwaiting)) <= (0).Zero and (1).NzNat + (-1).NzInt * rr(var(pemergencywaiting)) <= (0).Zero and (1).NzNat + (
    -1).NzInt * rr(var(pemergencylowering)) <= (0).Zero and (2).NzNat + (-1).NzInt * rr(var(papproaching)) <= (0).Zero
    and (2).NzNat + (-1).NzInt * rr(var(plowering)) <= (0).Zero and (3).NzNat + (-1).NzInt * rr(var(pfar)) <= (0).Zero
    and (-1).NzInt + rr(var(pverynear)) <= (0).Zero and (-1).NzInt + rr(var(pwaiting)) <= (0).Zero and (-1).NzInt + rr(
    var(pemergencywaiting)) <= (0).Zero and (-1).NzInt + rr(var(pemergencylowering)) <= (0).Zero and (-2).NzInt + rr(
    var(papproaching)) <= (0).Zero and (-2).NzInt + rr(var(plowering)) <= (0).Zero and (-3).NzInt + rr(var(pfar)) <= (
    0).Zero and rr(var(pfar)) + (-1).NzInt * rr(var(pwalkingsensor)) <= (0).Zero and rr(var(pverynear)) + (-1).NzInt *
    rr(var(papproaching)) <= (0).Zero and rr(var(pverynear)) + (-1).NzInt * rr(var(pemergencylowering)) <= (0).Zero and
    rr(var(pfar)) + rr(var(papproaching)) + (-1).NzInt * rr(var(pwalkingsensor)) <= (0).Zero and rr(var(papproaching))
    + (-1).NzInt * rr(var(pverynear)) + (-1).NzInt * rr(var(pwaiting)) <= (0).Zero and rr(var(pfar)) + rr(var(
    papproaching)) + (-1).NzInt * rr(var(pverynear)) + (-1).NzInt * rr(var(pwalkingsensor)) <= (0).Zero and (-1).NzInt
    * rr(var(pfar)) <= (0).Zero and (-1).NzInt * rr(var(papproaching)) <= (0).Zero and (-1).NzInt * rr(var(pverynear))
    <= (0).Zero and (-1).NzInt * rr(var(pwalkingsensor)) <= (0).Zero and (-1).NzInt * rr(var(pwalkinghouse)) <= (
    0).Zero and (-1).NzInt * rr(var(pwaiting)) <= (0).Zero and (-1).NzInt * rr(var(plowering)) <= (0).Zero and (
    -1).NzInt * rr(var(pemergencywaiting)) <= (0).Zero and (-1).NzInt * rr(var(pemergencylowering)) <= (0).Zero)

Solution 2
rewrites: 596942 in 292ms cpu (292ms real) (2038652 rewrites/second)
result State: Yes((intruder : choosehouse : choosing -> walkinghouse, intruder : ! powercut : walkinghouse ->
    intruderdone), (1).NzNat + (-1).NzInt * rr(var(pverynear)) <= (0).Zero and (1).NzNat + (-1).NzInt * rr(var(
    pwaiting)) <= (0).Zero and (1).NzNat + (-1).NzInt * rr(var(pemergencywaiting)) <= (0).Zero and (1).NzNat + (
    -1).NzInt * rr(var(pemergencylowering)) <= (0).Zero and (2).NzNat + (-1).NzInt * rr(var(papproaching)) <= (0).Zero
    and (2).NzNat + (-1).NzInt * rr(var(plowering)) <= (0).Zero and (3).NzNat + (-1).NzInt * rr(var(pfar)) <= (0).Zero
    and (-1).NzInt + rr(var(pverynear)) <= (0).Zero and (-1).NzInt + rr(var(pwaiting)) <= (0).Zero and (-1).NzInt + rr(
    var(pemergencywaiting)) <= (0).Zero and (-1).NzInt + rr(var(pemergencylowering)) <= (0).Zero and (-2).NzInt + rr(
    var(papproaching)) <= (0).Zero and (-2).NzInt + rr(var(plowering)) <= (0).Zero and (-3).NzInt + rr(var(pfar)) <= (
    0).Zero and rr(var(pfar)) + (-1).NzInt * rr(var(pwalkinghouse)) <= (0).Zero and rr(var(pverynear)) + (-1).NzInt *
    rr(var(papproaching)) <= (0).Zero and rr(var(pverynear)) + (-1).NzInt * rr(var(pemergencylowering)) <= (0).Zero and
    rr(var(pfar)) + rr(var(papproaching)) + (-1).NzInt * rr(var(pwalkinghouse)) <= (0).Zero and rr(var(papproaching)) +
    (-1).NzInt * rr(var(pverynear)) + (-1).NzInt * rr(var(pwaiting)) <= (0).Zero and rr(var(pfar)) + rr(var(
    papproaching)) + (-1).NzInt * rr(var(pverynear)) + (-1).NzInt * rr(var(pwalkinghouse)) <= (0).Zero and (-1).NzInt *
    rr(var(pfar)) <= (0).Zero and (-1).NzInt * rr(var(papproaching)) <= (0).Zero and (-1).NzInt * rr(var(pverynear)) <=
    (0).Zero and (-1).NzInt * rr(var(pwalkingsensor)) <= (0).Zero and (-1).NzInt * rr(var(pwalkinghouse)) <= (0).Zero
    and (-1).NzInt * rr(var(pwaiting)) <= (0).Zero and (-1).NzInt * rr(var(plowering)) <= (0).Zero and (-1).NzInt * rr(
    var(pemergencywaiting)) <= (0).Zero and (-1).NzInt * rr(var(pemergencylowering)) <= (0).Zero)

Solution 3
rewrites: 1173229 in 552ms cpu (552ms real) (2123679 rewrites/second)
result State: Yes((intruder : choosesensor : choosing -> walkingsensor, intruder : breaksensor : walkingsensor ->
    intruderdone), (1).NzNat + (-1).NzInt * rr(var(pverynear)) <= (0).Zero and (1).NzNat + (-1).NzInt * rr(var(
    pwaiting)) <= (0).Zero and (1).NzNat + (-1).NzInt * rr(var(pemergencywaiting)) <= (0).Zero and (1).NzNat + (
    -1).NzInt * rr(var(pemergencylowering)) <= (0).Zero and (2).NzNat + (-1).NzInt * rr(var(papproaching)) <= (0).Zero
    and (2).NzNat + (-1).NzInt * rr(var(plowering)) <= (0).Zero and (3).NzNat + (-1).NzInt * rr(var(pfar)) <= (0).Zero
    and (-1).NzInt + rr(var(pverynear)) <= (0).Zero and (-1).NzInt + rr(var(pwaiting)) <= (0).Zero and (-1).NzInt + rr(
    var(pemergencywaiting)) <= (0).Zero and (-1).NzInt + rr(var(pemergencylowering)) <= (0).Zero and (-2).NzInt + rr(
    var(papproaching)) <= (0).Zero and (-2).NzInt + rr(var(plowering)) <= (0).Zero and (-3).NzInt + rr(var(pfar)) <= (
    0).Zero and rr(var(pverynear)) + (-1).NzInt * rr(var(papproaching)) <= (0).Zero and rr(var(pverynear)) + (-1).NzInt
    * rr(var(pemergencylowering)) <= (0).Zero and rr(var(pwalkingsensor)) + (-1).NzInt * rr(var(pfar)) <= (0).Zero and
    (-1).NzInt * rr(var(pfar)) <= (0).Zero and (-1).NzInt * rr(var(papproaching)) <= (0).Zero and (-1).NzInt * rr(var(
    pverynear)) <= (0).Zero and (-1).NzInt * rr(var(pwalkingsensor)) <= (0).Zero and (-1).NzInt * rr(var(
    pwalkinghouse)) <= (0).Zero and (-1).NzInt * rr(var(pwaiting)) <= (0).Zero and (-1).NzInt * rr(var(plowering)) <= (
    0).Zero and (-1).NzInt * rr(var(pemergencywaiting)) <= (0).Zero and (-1).NzInt * rr(var(pemergencylowering)) <= (
    0).Zero)

...
```

## Dinning Philosophers

It is an extension of the well-known dining philosopher problem extended with
clocks, slightly modified in [3] to give some agents a choice in selecting
actions. The system consists of $2n+1$ agents (_i.e.,_ timed automata), with $n$
number of philosophers, $n$ number of forks, and a lackey who coordinates the
philosophers' access to the dinning room. The model has four values determining
the time constraints on the system behaviour: $T_1$, $T_2$, $E_1$, and $E_2$.
Each philosopher $i$ has to think at least $T_2$, and most $T_1$ time units
which is enforced by the guard $x_i \geq T_2$ and the invariant $x_i \leq T_1$, as
well philosopher $i$ is supposed to eat for at most $E_i$ ($x_i \leq E_1$) and
at least $E_2$ time units ($x_i \geq E_2$).

<figure style="text-align: center;">
  <img src="./images/philosopher.png" width="350"/>
  <figcaption style="font-weight: bold; border: none;">i-th Philosopher Automaton</figcaption>
</figure>

<figure style="text-align: center;">
  <img src="./images/fork.png" width="250"/>
  <figcaption style="font-weight: bold;">i-th Fork Automaton</figcaption>
</figure>

<figure style="text-align: center;">
  <img src="./images/lackey.png" width="350"/>
  <figcaption style="font-weight: bold;">Lackey Automaton</figcaption>
</figure>

We consider the property $\alpha$ reported in [3]. Such a property states that
a lackey has a strategy for letting the odd philosopher (but the last one) to
eat together before a deadline. Moreover, all the sensible locations of those
philosophers are reached. Maude solves all the instances reported in [3]
faster when using the `dsrew` command. 
Below the output for $p=5$.

```
Maude> dsrew [1] wrap(init, spec) using check .
dsrewrite [1] in DP : wrap(init, spec) using check .

Solution 1
rewrites: 818110 in 524ms cpu (527ms real) (1558390 rewrites/second)
result State: Yes((lackey : ? in(0) : l(0) -> l(1), lackey : ? in(1) : l(1) -> l(2), lackey : ? in(2) : l(2) -> l(1)),
    rr(var(T2)) + (-1).NzInt * rr(var(E2)) < (0).Zero and (-1).NzInt * rr(var(E1)) + (-1).NzInt * rr(var(E2)) < (
    0).Zero and (-1).NzInt * rr(var(E2)) + (-2).NzInt * rr(var(E1)) < (0).Zero and rr(var(T2)) + (-1).NzInt * rr(var(
    E1)) + (-1).NzInt * rr(var(E2)) < (0).Zero and rr(var(T2)) + (-1).NzInt * rr(var(E2)) + (-2).NzInt * rr(var(E1)) <
    (0).Zero and (-1).NzInt * rr(var(E2)) < (0).Zero and rr(var(T2)) + (-1).NzInt * rr(var(T1)) <= (0).Zero and rr(var(
    E2)) + (-1).NzInt * rr(var(E1)) <= (0).Zero and (-1).NzInt * rr(var(T1)) <= (0).Zero and (-1).NzInt * rr(var(T2))
    <= (0).Zero and (-1).NzInt * rr(var(E1)) <= (0).Zero and (-1).NzInt * rr(var(E2)) <= (0).Zero)
```

Below, the time reported when executing the command `dsrew` with subsumption
(strategy `check`) and without subsumption (strategy `check-no-sub`). 

| instance | time     | time2  |
| -------- | -------- | ------ |
| 2        | 46       | 40     |
| 3        | 63       | 50     |
| 4        | 538      | 206    |
| 5        | 2323     | 512    |
| 6        | 27045    | 6708   |
| 7        | 11317210 | 159275 |


More interesting, different from
[3], we can handle universal properties. Using the specification
$\langle\langle lackey \rangle\rangle\, (live \wedge critical)$ where
$$critical = \forall \mathsf{G}\, (\neg eat(0) \vee \neg eat(1))$$
and
$$live = \forall \mathsf{G}\, ((\exists \mathsf{F}\, eat(0)) \wedge (\exists \mathsf{F}\, eat(1)))$$
we can completely synthesize the automata for the lackey that satisfies such a
specification. We start with the completely non-deterministic Lackey in the figure below,
and the synthesized strategy is the following:

<figure style="text-align: center;">
  <img src="./images/ND-lackey.png" width="350"/>
  <figcaption style="font-weight: bold;">Gate Automaton</figcaption>
</figure>

```
Solution 1
rewrites: 259780718 in 180707ms cpu (181094ms real) (1437573 rewrites/second)
result State: Yes((lackey : ? in(0) : l(0) -> l(1), lackey : ? in(1) : l(2) -> l(3), lackey : ? out(0) : l(1) -> l(2),
    lackey : ? out(1) : l(3) -> l(0)), (-1).NzInt * rr(var(T1)) <= (0).Zero and (-1).NzInt * rr(var(T2)) <= (0).Zero
    and (-1).NzInt * rr(var(E1)) <= (0).Zero and (-1).NzInt * rr(var(E2)) <= (0).Zero)
```

## Voting System

We consider the voting model in [4] , inspired by the election procedures in Estonia.
There are two agents/automata schemes: the voter (V)
and the election authority (EA)

<figure style="text-align: center;">
  <img src="./images/voting.png" height="350"/>
  <figcaption style="font-weight: bold;">Voting System</figcaption>
</figure>


The voter (V) needs to register first, selecting one of the three voting
modalities: postal vote by mail, e-vote over the internet, or a traditional
paper ballot at a polling station. The EA accepts V's registration by
synchronizing with the registration transition. It then proceeds to send a
voting package appropriate for V's chosen modality, \eg a postal ballot for
voting by mail, e-voting credentials, or the address of the local election
office. After receiving the package, V casts a vote for the selected candidate,
which is registered by EA. The atomic proposition _vi_ denotes that V voted for
candidate i. The EA is subject to some timed constraints: it accepts votes by
mail between times 1 and 7, by Internet between 6 and 9, and at the polling
station between 10 and 11. The ballot is closed at time 11.  A voter must be
registered for a modality before its respective voting period

The results comparing this performance of Maude and the implementation 
of this system in Imitator are in `voting.csv`. 

## References

[1] Étienne André. 2019. What’s decidable about parametric timed automata?
Int. J. Softw. Tools Technol. Transf. 21, 2 (2019), 203–219.
https://doi.org/10.1007/s10009-017-0467-0

[2] Étienne André, Michal Knapik, Wojciech Penczek, and Laure Petrucci. 2016.
Controlling Actions and Time in Parametric Timed Automata. In 16th International
Conference on Application of Concurrency to System Design, ACSD 2016, Torun,
Poland, June 19-24, 2016, Jörg Desel and Alex Yakovlev (Eds.). IEEE Computer
Society, 45–54. https://doi.org/10.1109/ACSD.2016.20

[3] Magdalena Kacprzak, Artur Niewiadomski, Wojciech Penczek, and Andrzej
Zbrzezny. 2023. SMT-Based Satisfiability Checking of Strategic Metric Temporal
Logic. In Frontiers in Artificial Intelligence and Applications, Kobi Gal, Ann
Nowé, Grzegorz J. Nalepa, Roy Fairstein, and Roxana Rădulescu (Eds.). IOS Press.
https://doi.org/10.3233/FAIA230394

[4] Jaime Arias, Wojciech Jamroga, Wojciech Penczek, Laure Petrucci, Teofil Sidoruk:
Strategic (Timed) Computation Tree Logic. AAMAS 2023: 382-390

