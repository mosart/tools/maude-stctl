--- Modified from Maude-SE
--- the smtCheck function is not defined here

load smt .

--- Identifiers for variables
fmod VAR-ID is
    sort SMTVarId .
    op errorID : -> SMTVarId . --- For meta-level operations 
endfm

fmod BOOLEAN-EXPR is
  including VAR-ID .
  including BOOLEAN .

  sort BooleanExpr .
  subsort Boolean < BooleanExpr .
  
  sort BooleanVar .
  subsort BooleanVar < BooleanExpr .
  op b : SMTVarId -> BooleanVar [ctor] .

  op not_      : BooleanExpr -> BooleanExpr [ditto] .
  op _and_     : BooleanExpr BooleanExpr -> BooleanExpr [ditto] .
  op _xor_     : BooleanExpr BooleanExpr -> BooleanExpr [ditto] .
  op _or_      : BooleanExpr BooleanExpr -> BooleanExpr [ditto] .
  op _implies_ : BooleanExpr BooleanExpr -> BooleanExpr [ditto] .

  op _===_  : BooleanExpr BooleanExpr -> BooleanExpr [ditto] .
  op _=/==_ : BooleanExpr BooleanExpr -> BooleanExpr [ditto] .
  op _?_:_  : BooleanExpr BooleanExpr BooleanExpr -> BooleanExpr [ditto] .
endfm

fmod INTEGER-EXPR is
  including VAR-ID .
  protecting BOOLEAN-EXPR .
  including INTEGER .

  sort IntegerExpr .
  subsort Integer < IntegerExpr .

  sort IntegerVar .
  subsort IntegerVar < IntegerExpr .
  op i : SMTVarId -> IntegerVar [ctor] .

  op -_    : IntegerExpr -> IntegerExpr [ditto] .
  op _+_   : IntegerExpr IntegerExpr -> IntegerExpr [ditto] .
  op _*_   : IntegerExpr IntegerExpr -> IntegerExpr [ditto] .
  op _-_   : IntegerExpr IntegerExpr -> IntegerExpr [ditto] .
  op _div_ : IntegerExpr IntegerExpr -> IntegerExpr [ditto] .
  op _mod_ : IntegerExpr IntegerExpr -> IntegerExpr [ditto] .

  op _<_  : IntegerExpr IntegerExpr -> BooleanExpr [ditto] .
  op _<=_ : IntegerExpr IntegerExpr -> BooleanExpr [ditto] .
  op _>_  : IntegerExpr IntegerExpr -> BooleanExpr [ditto].
  op _>=_ : IntegerExpr IntegerExpr -> BooleanExpr [ditto] .

  op _===_  : IntegerExpr IntegerExpr -> BooleanExpr [ditto] .
  op _=/==_ : IntegerExpr IntegerExpr -> BooleanExpr [ditto] .
  op _?_:_  : BooleanExpr IntegerExpr IntegerExpr -> IntegerExpr [ditto] .

  op _divisible_ : IntegerExpr IntegerExpr -> BooleanExpr [ditto] .
endfm

fmod REAL-EXPR is
  including VAR-ID .
  protecting BOOLEAN-EXPR .
  including REAL .

  sort RealExpr .
  subsort Real < RealExpr .

  sort RealVar .
  subsort RealVar < RealExpr .
  op r : SMTVarId -> RealVar [ctor] .

  op -_  : RealExpr -> RealExpr [ditto] .
  op _+_ : RealExpr RealExpr -> RealExpr [ditto] .
  op _*_ : RealExpr RealExpr -> RealExpr [ditto] .
  op _-_ : RealExpr RealExpr -> RealExpr [ditto] .
  op _/_ : RealExpr RealExpr -> RealExpr [ditto] .

  op _<_  : RealExpr RealExpr -> BooleanExpr [ditto] .
  op _<=_ : RealExpr RealExpr -> BooleanExpr [ditto] .
  op _>_  : RealExpr RealExpr -> BooleanExpr [ditto] .
  op _>=_ : RealExpr RealExpr -> BooleanExpr [ditto] .

  op _===_  : RealExpr RealExpr -> BooleanExpr [ditto] .
  op _=/==_ : RealExpr RealExpr -> BooleanExpr [ditto] .
  op _?_:_  : BooleanExpr RealExpr RealExpr -> RealExpr [ditto] .
endfm

fmod REAL-INTEGER-EXPR is
  protecting INTEGER-EXPR .
  protecting REAL-EXPR .
  including REAL-INTEGER .

  op toReal    : IntegerExpr -> RealExpr [ditto] .
  op toInteger : RealExpr -> IntegerExpr [ditto] .
  op isInteger : RealExpr -> BooleanExpr [ditto] .
endfm


fmod SATISFYING-ASSIGNMENTS is
  protecting BOOLEAN-EXPR .
  protecting INTEGER-EXPR .
  protecting REAL-EXPR .
  protecting REAL-INTEGER-EXPR .

  sort SatAssignment .
  op _|->_ : IntegerVar Integer -> SatAssignment [ctor] .
  op _|->_ : BooleanVar Boolean -> SatAssignment [ctor] .
  op _|->_ : RealVar Real -> SatAssignment [ctor] .

  sort SatAssignmentSet .
  subsort SatAssignment < SatAssignmentSet .
  op empty : -> SatAssignmentSet [ctor] .
  op _,_ : SatAssignmentSet SatAssignmentSet -> SatAssignmentSet [ctor comm assoc id: empty] .
endfm

fmod META-SMT-INTERFACE is
  protecting META-LEVEL .

  var M           : Module .
  var C           : Constant .
  var V           : Variable .
  var F           : Qid .
  vars T T'       : Term .
  var TY          : Type .
  var TL TAL TAL' : TermList .
  var NTL         : NeTermList .


 op isBuiltinVar : Module Term ~> Bool .
 ceq isBuiltinVar(M, T) 
   = sortLeq(M, TY, 'BooleanVar) or 
     sortLeq(M, TY, 'IntegerVar) or sortLeq(M, TY, 'RealVar)
  if TY := leastSort(M, T) .
endfm
