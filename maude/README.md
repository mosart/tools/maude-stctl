### Auxiliary modules and definitions

- [_alt-smt_](./maude/alt-smt.maude): Representation of SMT Boolean, Integer and Real expressions as
  Maude's terms. SMT variables are encoded as terms of the form
  and `r(x)` where `x` is a term of sort `SMTVarId` (a variable identifier).
- [_smt-check_](./maude/smt-check.maude): Interface with the SMT solver.
- [_fme_](./maude/fme.maude): Fourier-Motzkin Elimination procedure for eliminating existentially
  quantified variables.

### Modules for PTAs

- [_syntax_](./maude/syntax.maude): Syntax for defining networks of parametric timed automata.
- [_state_](./maude/state.maude): Sorts and constructors for defining the state of automata.

### Modules for STCTL formulas

- [_stctl-syntax_](./maude/stctl-syntax.maude): Syntax of STCTL (and SCTL) formulas.
- [_stctl-semantics_](./maude/stctl-semantics.maude): Rules and rewriting strategy giving meaning to formulas.

### Experiments

See [here](./maude/examples/README.md) and [here](../EF-benchmarks) for further
information.

