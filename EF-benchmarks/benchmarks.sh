#!/bin/bash

# Imitator binary
imitator="/opt/imitator-v3.3.0"

# maude binary
maude="/opt/maude-3.4/maude -no-banner -batch"

# nptav2maude parser
BASE_DIR="$(dirname "$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)")"
parser="${BASE_DIR}/nptav2maude/app.py"
theory_folder="${BASE_DIR}/maude"

# folder containing the folder
model_folder="models"

# modes
#modes=("folding" "no-folding" "search")
modes=("srew" "dsrew" "srew-ws" "dsrew-ws")
imi_modes=("EFwitness")

# run imitator
function run_imitator {
  local model=$1
  local property=$2
  local timeout_value=$3
  timeout $timeout_value $imitator "${model}" "${property}" -output-prefix "${property}"
}

# run parser pta2maude
function parser_files {
  local model=$1
  local method=$2

  echo "parsing" $model "with method" "$method" "..."
  python3 $parser --input "${model_folder}/${model}.imi" --output "${model_folder}/${model}.${method}.maude" --method "${method}"
}

# run Maude
function run_maude {
  local filename=$1
  local timeout_value=$2
  timeout "$timeout_value" ${maude} "${filename}"
}

# create a folder
function create_folder {
  local path=$1

  if [ ! -d "$path" ]; then
    mkdir -p "$path"
  fi
}

function run_benchmark {
  local model=$1
  local timeout=$2

  # create folder to save the output of the model
  output_folder="logs/${model}"
  create_folder "${output_folder}"
  cp ${theory_folder}/*.maude "${output_folder}"

  # 1) generate the maude model with the parser
  #for mode in ${modes[@]}; do
  #  parser_files $model "$mode"
  #done

  # 2) get the locations of the full model
  locations="${model_folder}/${model}_loc.txt"

  while read -r a l; do
    # 3) run imitator
    for imi_mode in ${imi_modes[@]}; do
      prop_file="${imi_mode}.imiprop"
      model_path="${model_folder}/${model}.imi"

      new_prop_path="${output_folder}/${model}-${prop_file}.${l}"
      if [[ ! -f "${new_prop_path}.res" ]]; then
        sed "s/<replace>/loc[${a}] = ${l}/" "${model_folder}/${prop_file}" >"${new_prop_path}"
        echo "Running (timeout: ${timeout}) ${model} with ${new_prop_path}"
        run_imitator "${model_path}" "${new_prop_path}" "${timeout}"
      fi
    done

    # 4) run maude
    for mode in ${modes[@]}; do
      maude_file="${model}.${mode}.maude"
      new_file="${output_folder}/${maude_file}.${l}"

      for i in {0..0}; do
        # active/deactive FME technique
        #sed -i "/eq module(Q)/c\  eq module(Q) = module(${i}, Q) ." "${output_folder}/analysis.maude"

        suffix=$([[ $i -eq 1 ]] && echo ".FEM" || echo "")
        output_maude_file="${new_file}${suffix}.res"

        if [[ ! -f "${output_maude_file}" ]]; then
          sed "s/<replace>/${a} @ ${l}/" "${model_folder}/${maude_file}" >"${new_file}"
          echo "Running Maude (timeout: ${timeout}, method: ${mode}, FME: ${i}) with ${new_file}"
          run_maude "${new_file}" "${timeout}" >"${output_maude_file}"
        fi
      done
    done
  done <$locations

  # 5) generate file with locations where maude finishes
  location_file="${output_folder}/locations.txt"

  files=$(cd "${output_folder}" && grep -Rli "rewrite" ".")
  success_locations=$(echo "$files" | tr " " "\n" | cut -d. -f5 | sort -u)
  echo "$success_locations" >"${location_file}"
  sed -zi 's/\n$//g' ${location_file}
  sed -zi 's/^\n//g' ${location_file}
}

# Print a help message.
function usage() {
  echo "Usage: $0 -m MODEL -t TIMEOUT" 1>&2
}

# Exit with error.
exit_abnormal() {
  usage
  exit 1
}

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------

TIMEOUT=""
MODEL=""

while [[ "$#" -gt 0 ]]; do
  case "$1" in
  -m | --model)
    MODEL="$2"
    shift
    ;;
  -t | --timeout)
    TIMEOUT="$2"
    shift
    ;;
  *) exit_abnormal ;;
  esac
  shift
done

if [ "$MODEL" = "" ] || [ "$TIMEOUT" = "" ]; then
  exit_abnormal
fi

# use "all" for run all the experiments
if [ "$MODEL" = "all" ]; then
  MODEL=$(find "$model_folder" -name "*_loc.txt" -type f -exec basename {} \;)
fi

# run benchmark
for m in $(echo "$MODEL"); do
  model="${m%%_loc*}";
  run_benchmark "${model}" "${TIMEOUT}"
done



