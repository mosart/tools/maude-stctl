#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from IPython.display import display, HTML

style = open("./.jupyter/style.css").read()
display(HTML("<style>%s</style>" % style))


# In[ ]:


import os
import re
import csv
import pandas as pd
import math
import plotly.io as pio
import plotly.express as px
import plotly.graph_objects as go

pio.kaleido.scope.mathjax = None
pio.kaleido.scope.chromium_args += ("--single-process",)

# timeout (10 minutes)
timeout_m = 10
TIMEOUT = timeout_m * 60 * 1000  # ms
ZERO = 10e-2

# models to analyse
models = [
    "accel-1000",
    "ATM",
    "blowup-200",
    "coffee",
    "fischer",
    "gear-1000",
    "IMPO",
    "jobshop_4_4",
    "NuclearPlant",
    "packaging",
    "Pipeline_KP12_2_3",
    "RCP",
    "simop2",
    "tgc",
    "WFAS_BBLS15",
]

# csv with results
csv_filename = "results.csv"

# folder with tools results
log_folder = "logs"

# folder with the mdoels
models_folder = "models"

# filenames
imitator_files = [
    {"name": "witness", "file": "{model}-EFwitness.imiprop.{location}.res"},
    {"name": "synth", "file": "{model}-EFsynth.imiprop.{location}.res"},
]
maude_files = [
    {"name": "synthesis", "file": "{model}.no-folding.maude.{location}.res"},
    {"name": "folding", "file": "{model}.folding.maude.{location}.res"},
    {"name": "search", "file": "{model}.search.maude.{location}.res"},
]

# tools labels for the tables
imitator_labels = [f'imitator_{f["name"]}_time(ms)' for f in imitator_files]
maude_labels = []
for f in maude_files:
    prefix = f'maude_{f["name"]}'
    maude_labels.append(f"{prefix}_rewrites")
    maude_labels.append(f"{prefix}_cpu(ms)")
    maude_labels.append(f"{prefix}_real(ms)")

info_fieldnames = [
    "ptas",
    "clocks",
    "parameters",
    "actions",
    "locations",
    "transitions",
    "location_reached",
]
tools_fieldnames = imitator_labels + maude_labels
compare_fieldnames = imitator_labels + [l for l in maude_labels if "real(ms)" in l]


# # Generate CSV file with data
# 

# In[ ]:


def format_unit(value, unit):
    if unit == "ms":
        return float(value)
    elif unit == "second" or unit == "seconds":
        return float(value) * 1000
    else:
        raise Exception(f"Unit {unit} is not supported")


def parse_imi_file(filename_file, model, loc):
    # regex
    regex_imitator = re.compile(r"Total computation time\s*:\s*(\d+(?:\.\d+)?) (\w+)")
    regex_ptas = re.compile(r"Number of IPTAs\s*:\s*(\d+)")
    regex_clocks = re.compile(r"Number of clocks\s*:\s*(\d+)")
    regex_parameters = re.compile(r"Number of parameters\s*:\s*(\d+)")
    regex_actions = re.compile(r"Number of actions\s*:\s*(\d+)")
    regex_locations = re.compile(r"Total number of locations\s*:\s*(\d+)")
    regex_transitions = re.compile(r"Total number of transitions\s*:\s*(\d+)")

    output = {"time": None}

    # search in imitator file
    imi_filename = filename_file.format(model=model, location=loc)
    file_path = os.path.join(log_folder, model, imi_filename)

    if not os.path.exists(file_path):
        return output

    with open(file_path, "r") as imi_file:
        imi_content = imi_file.read()

        # get imitator times
        imi_search = regex_imitator.search(imi_content)
        if imi_search is not None:
            imi_time, imi_unit = imi_search.groups()
            output["time"] = f"{format_unit(imi_time, imi_unit)}"

        # get model's information
        output["nb_ptas"] = regex_ptas.search(imi_content).group(1)
        output["nb_clocks"] = regex_clocks.search(imi_content).group(1)
        output["nb_parameters"] = regex_parameters.search(imi_content).group(1)
        output["nb_actions"] = regex_actions.search(imi_content).group(1)
        output["nb_locations"] = regex_locations.search(imi_content).group(1)
        output["nb_transitions"] = regex_transitions.search(imi_content).group(1)

        return output


def parse_maude_file(filename_file, model, loc):
    regex_maude = re.compile(r"rewrites: (\d+) in (\d+)(\w+) cpu \((\d+)(\w+) real\)")
    output = {"rewrites": None, "cpu": None, "real": None}

    # search in maude file
    maude_filename = filename_file.format(model=model, location=loc)
    file_path = os.path.join(log_folder, model, maude_filename)

    if not os.path.exists(file_path):
        return output

    with open(file_path, "r") as rl_file:
        maude_content = regex_maude.search(rl_file.read())

        if maude_content is not None:
            (rewrites, cpu, cpu_unit, real, real_unit) = maude_content.groups()
            output["rewrites"] = rewrites
            output["cpu"] = f"{format_unit(cpu, cpu_unit)}"
            output["real"] = f"{format_unit(real, real_unit)}"
        return output


def generate_csv():
    with open(csv_filename, "w") as csv_file:
        fieldnames = ["model"] + info_fieldnames + tools_fieldnames
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for model in models:
            with open(os.path.join(models_folder, f"{model}_loc.txt")) as loc_file:
                locations = [l.replace("\n", "").split(" ")[1] for l in loc_file]
                for loc in locations:
                    try:
                        results = {"model": model, "location_reached": loc}
                        for f in imitator_files:
                            imitator_info = parse_imi_file(f["file"], model, loc)
                            results[f'imitator_{f["name"]}_time(ms)'] = imitator_info[
                                "time"
                            ]

                            # get model information
                            if "nb_ptas" in imitator_info:
                                results["ptas"] = imitator_info["nb_ptas"]
                                results["clocks"] = imitator_info["nb_clocks"]
                                results["parameters"] = imitator_info["nb_parameters"]
                                results["actions"] = imitator_info["nb_actions"]
                                results["locations"] = imitator_info["nb_locations"]
                                results["transitions"] = imitator_info["nb_transitions"]

                        for f in maude_files:
                            maude_info = parse_maude_file(f["file"], model, loc)
                            prefix = f'maude_{f["name"]}'
                            results[f"{prefix}_rewrites"] = maude_info["rewrites"]
                            results[f"{prefix}_cpu(ms)"] = maude_info["cpu"]
                            results[f"{prefix}_real(ms)"] = maude_info["real"]

                        # save info
                        writer.writerow(results)
                    except Exception as e:
                        print(e)


# In[ ]:


generate_csv()


# # Analyse Data
# 

# In[ ]:


def highlight_cell(
    df, latex=False, subset=compare_fieldnames, null_color="red", min_color="green"
):
    color_fmt = "color: {{{color}}}; bfseries: ;" if latex else "color: {color};"
    return (
        df.style.highlight_min(
            axis=1, props=color_fmt.format(color=min_color), subset=subset
        )
        .highlight_null(props=color_fmt.format(color=null_color))
        .format(na_rep="TO", precision=1)
    )


# In[ ]:


def export_to_latex(
    df,
    filename,
    highlight=True,
    subset=compare_fieldnames,
    null_color="BrickRed",
    min_color="OliveGreen",
):
    base_style = (
        highlight_cell(df, True, subset, null_color, min_color)
        if highlight
        else df.style
    )
    s = base_style.format_index("\\textbf{{{}}}", escape="latex", axis=1).hide(
        axis="index"
    )

    return s.to_latex(os.path.join("images", f"{filename}.tex"), hrules=True)


# In[ ]:


df = pd.read_csv(csv_filename)
df = df.set_index("model")
df


# In[ ]:


dfs_times = df[["location_reached"] + compare_fieldnames].reset_index()
export_to_latex(dfs_times, "table-times")
times_table = highlight_cell(dfs_times)
times_table.to_html(open(f"images/table-times.html", "w"))
times_table


# In[ ]:


df_TO = (
    dfs_times[compare_fieldnames]
    .isnull()
    .groupby([dfs_times["model"]])
    .sum()
    .astype(int)
    .reset_index()
)
export_to_latex(df_TO, "table-TOs", False)
df_TO


# In[ ]:


df_model_info = df.drop_duplicates(
    subset=info_fieldnames[:-1], keep="last"
).reset_index()
df_model_info = df_model_info.drop(
    labels=["location_reached"] + tools_fieldnames,
    axis=1,
)
df_model_info = df_model_info.sort_values(
    by="model", key=lambda col: col.str.lower(), ignore_index=True
)

export_to_latex(df_model_info, "table-params", False)
df_model_info


# # Plot
# 

# In[ ]:


def plot(df, y_axis, model_name):
    fig = go.Figure()

    min_value = ZERO
    max_value = TIMEOUT

    # colors and markers
    symbols = ["circle", "square", "diamond", "star"]
    colors = px.colors.qualitative.G10

    # replace non finished experiments with a dummy value, e.g. timeout
    model = df.fillna(max_value).loc[[model_name]]

    # replace 0.00 ms for 0.1 ms, we cannot plot log(0)
    model = model.replace(0.0, min_value)

    # results
    for i, f in enumerate(maude_files):
        maude_label = f["name"]
        fig.add_trace(
            go.Scatter(
                name=maude_label,
                x=model[f"maude_{maude_label}_real(ms)"],
                y=model[y_axis],
                text=model["location_reached"],
                mode="markers",
                marker=dict(
                    symbol=symbols[i],
                    color=colors[i],
                    size=14,
                    opacity=0.5,
                    line=dict(color="black", width=2),
                ),
            )
        )

    # timeout lines
    fig.add_trace(
        go.Scatter(
            x=[min_value - 0.2, max_value],
            y=[max_value - 0.2, max_value],
            mode="lines",
            line=dict(color="red", width=2, dash="dash"),
            showlegend=False,
        )
    )

    fig.add_trace(
        go.Scatter(
            x=[max_value - 0.2, max_value],
            y=[min_value - 0.2, max_value],
            mode="lines",
            line=dict(color="red", width=2, dash="dash"),
            showlegend=False,
        )
    )

    fig.add_annotation(
        x=0.55,
        y=0.99,
        xref="x domain",
        yref="y domain",
        text=f"timeout ({timeout_m} min.)",
        font=dict(size=18, color="black"),
        showarrow=False,
        opacity=0.5,
    )
    fig.add_annotation(
        x=0.99,
        y=0.05,
        xref="x domain",
        yref="y domain",
        text=f"timeout ({timeout_m} min.)",
        font=dict(size=18, color="black"),
        showarrow=False,
        opacity=0.5,
        textangle=90,
    )

    # compute axis bound
    offset = 0.4
    min_value_log = math.log10(min_value) - offset
    max_value_log = math.log10(max_value) + offset

    # identity line
    fig.add_trace(
        go.Scatter(
            x=[10**min_value_log, 10 ** (max_value_log)],
            y=[10**min_value_log, 10 ** (max_value_log)],
            mode="lines",
            line=dict(color="black", width=1),
            showlegend=False,
        )
    )

    # set axes
    fig.update_xaxes(
        type="log",
        showgrid=True,
        gridcolor="grey",
        griddash="dot",
        gridwidth=1,
        mirror=True,
        linewidth=1,
        linecolor="black",
        constrain="domain",
        range=[min_value_log, max_value_log],
        title="Maude (ms)",
        exponentformat="power",
        dtick=1,
        tick0=0,
        minor=dict(ticks="inside", ticklen=6, showgrid=True),
    )
    fig.update_yaxes(
        type="log",
        showgrid=True,
        gridcolor="grey",
        griddash="dot",
        gridwidth=1,
        mirror=True,
        linewidth=1,
        linecolor="black",
        scaleanchor="x",
        scaleratio=1,
        range=[min_value_log, max_value_log],
        title="Imitator (ms)",
        exponentformat="power",
        dtick=1,
        tick0=0,
        title_standoff=1,
        minor=dict(ticks="inside", ticklen=6, showgrid=True),
    )

    # set legend
    legend_options = dict(
        yanchor="top",
        y=0.99,
        xanchor="left",
        x=0.15,
        bordercolor="Black",
        borderwidth=1,
    )

    # set figure layout
    margin = margin = dict(l=20, r=20, t=40, b=20)
    fig.update_layout(
        width=700,
        height=600,
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend_title_text="Method",
        legend=legend_options,
        autosize=False,
        margin=margin,
        font=dict(size=20, family="Roboto", color="black"),
        showlegend=True,
        title=dict(
            text=f"<b>{model_name}</b>",
            font=dict(size=24),
            y=0.98,
            x=0.5,
            xanchor="center",
            yanchor="top",
        ),
    )

    fig.update_traces(
        hovertemplate="<b>%{text}</b><br><br>Maude: %{x} ms <br>Imitator: %{y} ms<extra></extra>"
    )

    return fig


# In[ ]:


# show a figure
plot(df, "imitator_witness_time(ms)", models[0])


# In[ ]:


for label in imitator_labels:
    method = label.split("_")[1]
    for m in models:
        try:
            fig = plot(df, label, m)
            filename = m.replace("_", "-")
            fig.write_html(f"images/{method}/{filename}.html")
            fig.write_image(f"images/{method}/{filename}.pdf", format="pdf")
        except:
            print(pio.kaleido.scope._std_error.getvalue().decode("utf8"))

