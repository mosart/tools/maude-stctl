load stctl-semantics .

smod MODEL is
    extending VERIFIER .

    ops controller0 controller1 controller2 controller3 gate0 gate1 gate2 gate3 train0 train1 train2 train3 : -> Location [ctor] .
    ops approach exit lower raise : -> SAction [ctor] .
    ops down inn out up : -> LAction [ctor] .
    ops controller gate train : -> Agent [ctor] .
    ops x y z : -> Clock [ctor] .
    ops a b c d e f : -> Parameter [ctor] .

    op Train : -> Automaton .
    eq Train = < train |
                   (@ train0 inv true : 
                      (when true sync ! approach do { x := 0 } goto train1)),
                   (@ train1 inv true : 
                      (when (x > a) local inn do { nothing } goto train2)),
                   (@ train2 inv true : 
                      (when true local out do { nothing } goto train3)),
                   (@ train3 inv true : 
                      (when (x < b) sync ! exit do { nothing } goto train0)) >
                  .

    op Gate : -> Automaton .
    eq Gate = < gate |
                   (@ gate0 inv true : 
                      (when true sync ! lower do { y := 0 } goto gate1)),
                   (@ gate1 inv true : 
                      (when ((c < y) and (y < d)) local down do { nothing } goto gate2)),
                   (@ gate2 inv true : 
                      (when true sync ! raise do { y := 0 } goto gate3)),
                   (@ gate3 inv true : 
                      (when ((c < y) and (y < d)) local up do { nothing } goto gate0)) >
                  .

    op Controller : -> Automaton .
    eq Controller = < controller |
                   (@ controller0 inv true : 
                      (when true sync ? approach do { z := 0 } goto controller1)),
                   (@ controller1 inv true : 
                      (when ((e < z) and (z < f)) sync ? lower do { nothing } goto controller2)),
                   (@ controller2 inv true : 
                      (when true sync ? exit do { z := 0 } goto controller3)),
                   (@ controller3 inv true : 
                      (when ((e < z) and (z < f)) sync ? raise do { nothing } goto controller0)) >
                  .

    op init : -> State .
    eq init = ( Controller, Gate, Train ) : 
              < 
                locs: (train @ train0, gate @ gate0, controller @ controller0)
                clocks: (x : 0, y : 0, z : 0)
                params: (a : rr(var(a)), b : rr(var(b)), c : rr(var(c)), d : rr(var(d)), e : rr(var(e)), f : rr(var(f)))
                gvars: empty > || 
                (rr(var(a)) >= 0/1 and rr(var(a)) <= rr(var(b)) and rr(var(c)) >= 0/1 and rr(var(c)) <= rr(var(d)) and rr(var(e)) >= 0/1 and rr(var(e)) <= rr(var(f)))
                : empty .
endsm

srew [1] wrap(init, EF (<replace>)) using check .


quit .

eof
