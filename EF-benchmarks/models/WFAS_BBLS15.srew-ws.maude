load stctl-semantics .

smod MODEL is
    extending VERIFIER .

    ops awake awake cont1 cont2 cont3 cont4 fail idle idle timeout : -> Location [ctor] .
    ops result1 result2 wakeup1 wakeup2 : -> SAction [ctor] .
    op noaction : -> LAction [ctor] . 
    ops controller sensor1 sensor2 : -> Agent [ctor] .
    ops x x1 x2 y : -> Clock [ctor] .
    ops p1 p2 : -> Parameter [ctor] .

    op Sensor1 : -> Automaton .
    eq Sensor1 = < sensor1 |
                   (@ idle inv true : 
                      (when true sync ! wakeup1 do { x1 := 0 } goto awake)),
                   (@ awake inv (x1 < 3/1) : 
                      (when true sync ! wakeup1 do { nothing } goto awake ,
                       when ((x1 > 2/1) and (x1 < 3/1)) sync ! result1 do { nothing } goto idle)) >
                  .

    op Sensor2 : -> Automaton .
    eq Sensor2 = < sensor2 |
                   (@ idle inv true : 
                      (when true sync ! wakeup2 do { x2 := 0 } goto awake)),
                   (@ awake inv (x2 < 17/1) : 
                      (when true sync ! wakeup2 do { nothing } goto awake ,
                       when ((x2 > 2/1) and (x2 < 3/1)) sync ! result2 do { nothing } goto idle ,
                       when ((x2 > 16/1) and (x2 < 17/1)) sync ! result2 do { nothing } goto idle)) >
                  .

    op Controller : -> Automaton .
    eq Controller = < controller |
                   (@ cont1 inv ((x < 2/1) and (y <= 20/1)) : 
                      (when true sync ? result1 do { nothing } goto fail ,
                       when true sync ? result2 do { nothing } goto fail ,
                       when (x < 2/1) sync ? wakeup1 do { nothing } goto cont2 ,
                       when (y = 20/1) local noaction do { nothing } goto timeout)),
                   (@ cont2 inv ((x <= p1) and (y <= 20/1)) : 
                      (when true sync ? result2 do { nothing } goto fail ,
                       when (x = p1) local noaction do { x := 0 } goto cont3 ,
                       when (x < p1) sync ? result1 do { x := 0 } goto cont3 ,
                       when (y = 20/1) local noaction do { nothing } goto timeout)),
                   (@ cont3 inv ((x < 2/1) and (y <= 20/1)) : 
                      (when true sync ? result1 do { nothing } goto fail ,
                       when true sync ? result2 do { nothing } goto fail ,
                       when (x < 2/1) sync ? wakeup2 do { nothing } goto cont4 ,
                       when (y = 20/1) local noaction do { nothing } goto timeout)),
                   (@ cont4 inv ((x <= p2) and (y <= 20/1)) : 
                      (when true sync ? result1 do { nothing } goto fail ,
                       when (x = p2) local noaction do { x := 0 ; y := 0 } goto cont1 ,
                       when (x < p2) sync ? result2 do { x := 0 ; y := 0 } goto cont1 ,
                       when (y = 20/1) local noaction do { nothing } goto timeout)),
                   (@ fail inv true : empty),
                   (@ timeout inv true : empty) >
                  .

    op init : -> State .
    eq init = ( Controller, Sensor1, Sensor2 ) : 
              <
                locs: (sensor1 @ idle, sensor2 @ idle, controller @ cont1)
                clocks: (x1 : 0, x2 : 0, x : 0, y : 0)
                params: (p1 : rr(var(p1)), p2 : rr(var(p2)))
                gvars: empty > || 
                ( rr(var(p1)) >= 0/1 and rr(var(p2)) >= 0/1 ) : empty .


endsm


srew [1] wrap(init, EF (<replace>)) using check-no-sub .


quit .

eof
