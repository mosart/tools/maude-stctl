load stctl-semantics .

smod MODEL is
    extending VERIFIER .

    ops active1 active2 check1 check2 cs1 cs2 idle1 idle2 locaccess1 locaccess2 obs1 obs2 obsviolation obswaiting : -> Location [ctor] .
    ops enter1 enter2 exit1 exit2 : -> SAction [ctor] .
    ops access1 access2 noaccess1 noaccess2 noaction try1 try2 update1 update2 : -> LAction [ctor] .
    ops observer proc1 proc2 : -> Agent [ctor] .
    ops x1 x2 : -> Clock [ctor] .
    op turn : -> GVar [ctor] .
    ops delta gamma : -> Parameter [ctor] .

    op Proc1 : -> Automaton .
    eq Proc1 = < proc1 |
                   (@ idle1 inv true : 
                      (when (turn = -1/1) local try1 do { x1 := 0 } goto active1)),
                   (@ active1 inv (x1 <= delta) : 
                      (when true local update1 do { turn := 1 ; x1 := 0 } goto check1)),
                   (@ check1 inv true : 
                      (when ((x1 >= gamma) and (turn = 1/1)) local access1 do { nothing } goto locaccess1 ,
                       when ((x1 >= gamma) and (turn < 1/1)) local noaccess1 do { nothing } goto idle1 ,
                       when ((x1 >= gamma) and (turn > 1/1)) local noaccess1 do { nothing } goto idle1)),
                   (@ locaccess1 inv true : 
                      (when true sync ! enter1 do { nothing } goto cs1)),
                   (@ cs1 inv true : 
                      (when true sync ! exit1 do { turn := -1 } goto idle1)) >
                  .

    op Proc2 : -> Automaton .
    eq Proc2 = < proc2 |
                   (@ idle2 inv true : 
                      (when (turn = -1/1) local try2 do { x2 := 0 } goto active2)),
                   (@ active2 inv (x2 <= delta) : 
                      (when true local update2 do { turn := 2 ; x2 := 0 } goto check2)),
                   (@ check2 inv true : 
                      (when ((x2 >= gamma) and (turn = 2/1)) local access2 do { nothing } goto locaccess2 ,
                       when ((x2 >= gamma) and (turn < 2/1)) local noaccess2 do { nothing } goto idle2 ,
                       when ((x2 >= gamma) and (turn > 2/1)) local noaccess2 do { nothing } goto idle2)),
                   (@ locaccess2 inv true : 
                      (when true sync ! enter2 do { nothing } goto cs2)),
                   (@ cs2 inv true : 
                      (when true sync ! exit2 do { turn := -1 } goto idle2)) >
                  .

    op Observer : -> Automaton .
    eq Observer = < observer |
                   (@ obswaiting inv true : 
                      (when true sync ? enter1 do { nothing } goto obs1 ,
                       when true sync ? enter2 do { nothing } goto obs2)),
                   (@ obs1 inv true : 
                      (when true sync ? exit1 do { nothing } goto obswaiting ,
                       when true sync ? enter2 do { nothing } goto obsviolation)),
                   (@ obs2 inv true : 
                      (when true sync ? exit2 do { nothing } goto obswaiting ,
                       when true sync ? enter1 do { nothing } goto obsviolation)),
                   (@ obsviolation inv true : empty) >
                  .

    op init : -> State .
    eq init = ( Observer, Proc1, Proc2 ) : 
              < 
                locs: (proc1 @ idle1, proc2 @ idle2, observer @ obswaiting)
                clocks: (x1 : 0, x2 : 0)
                params: (delta : rr(var(delta)), gamma : rr(var(gamma)))
                gvars: (turn : -1/1) > || 
                ( rr(var(delta)) >= 0/1 and rr(var(gamma)) >= 0/1) : empty . 
endsm

srew [1] wrap(init, EF (<replace>)) using check .



quit .

eof
