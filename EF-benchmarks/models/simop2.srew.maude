load stctl-semantics .

smod MODEL is
    extending VERIFIER .

    ops COM1 COM10 COM11 COM12 COM13 COM14 COM15 COM2 COM3 COM4 COM5 COM6 COM7 COM8 COM9 COMinit ENV1 ENV2 ENV4 ENV5 ENVinit NET1 NET2 NET3 NET4 NET5 NET6 NET7 PLC1 PLC3 PLC4 PLC5 PLC6 PLC7 PLC9 PLCinit RIO1 RIO10 RIO2 RIO3 RIO4 RIO5 RIO6 RIO7 RIO8 RIO9 : -> Location [ctor] .
    ops CNreq0 CNreq1 COMin0 COMin1 NCrep0 NCrep1 NRreq0 NRreq1 PLCbeg PLCout0 PLCout1 RNrep0 RNrep1 SIGin1 SIGout0 SIGout1 noaction : -> SAction [ctor] .
    op noaction : -> LAction .
    ops COM ENV NET PLC RIO : -> Agent [ctor] .
    ops COMclk ENVclk NETclk PLCclk RIOclk tt yy zz : -> Clock [ctor] .
    ops COMct COMd NETd PLCct PLCmtt RIOd SIGmrt : -> Parameter [ctor] .

    op Plc : -> Automaton .
    eq Plc = < PLC |
                   (@ PLCinit inv true : 
                      (when true sync ! PLCbeg do { PLCclk := 0 } goto PLC1)),
                   (@ PLC1 inv (PLCclk <= PLCct) : 
                      (when true sync ! COMin0 do { nothing } goto PLC1 ,
                       when (PLCclk >= PLCmtt) sync ! PLCout0 do { nothing } goto PLC3)),
                   (@ PLC3 inv (PLCclk <= PLCct) : 
                      (when (PLCclk = PLCct) local noaction do { PLCclk := 0 } goto PLC1 ,
                       when true sync ! COMin1 do { nothing } goto PLC6 ,
                       when true sync ! COMin0 do { nothing } goto PLC3)),
                   (@ PLC4 inv (PLCclk <= PLCct) : 
                      (when true sync ! COMin1 do { nothing } goto PLC4 ,
                       when (PLCclk >= PLCmtt) local noaction do { yy := 0 } goto PLC5)),
                   (@ PLC5 inv (yy <= 0/1) : 
                      (when (yy = 0/1) sync ! PLCout0 do { nothing } goto PLC6 ,
                       when true sync ! COMin1 do { nothing } goto PLC5)),
                   (@ PLC6 inv (PLCclk <= PLCct) : 
                      (when (PLCclk = PLCct) local noaction do { PLCclk := 0 } goto PLC7 ,
                       when true sync ! COMin1 do { nothing } goto PLC6)),
                   (@ PLC7 inv (PLCclk <= PLCct) : 
                      (when true sync ! COMin1 do { nothing } goto PLC7 ,
                       when (PLCclk >= PLCmtt) sync ! PLCout1 do { nothing } goto PLC9)),
                   (@ PLC9 inv (PLCclk <= PLCct) : 
                      (when (PLCclk = PLCct) local noaction do { PLCclk := 0 } goto PLC7 ,
                       when true sync ! COMin1 do { nothing } goto PLC9)) >
                  .

    op Com : -> Automaton .
    eq Com = < COM |
                   (@ COMinit inv true : 
                      (when true sync ? PLCbeg do { nothing } goto COM1)),
                   (@ COM1 inv true : 
                      (when true local noaction do { COMclk := 0 } goto COM2 ,
                       when true sync ? PLCout0 do { nothing } goto COM1)),
                   (@ COM2 inv (COMclk <= COMd) : 
                      (when (COMclk = COMd) sync ? CNreq0 do { nothing } goto COM3 ,
                       when true sync ? PLCout0 do { nothing } goto COM2)),
                   (@ COM3 inv (COMclk <= COMct) : 
                      (when true sync ? NCrep0 do { tt := 0 } goto COM4 ,
                       when true sync ? PLCout0 do { nothing } goto COM3 ,
                       when true sync ? NCrep1 do { tt := 0 } goto COM5)),
                   (@ COM4 inv (tt = 0/1) : 
                      (when (tt = 0/1) sync ? COMin0 do { nothing } goto COM6)),
                   (@ COM5 inv (tt = 0/1) : 
                      (when (tt = 0/1) sync ? COMin1 do { nothing } goto COM6)),
                   (@ COM6 inv (COMclk <= COMct) : 
                      (when (COMclk = COMct) local noaction do { COMclk := 0 } goto COM2 ,
                       when true sync ? PLCout0 do { nothing } goto COM6 ,
                       when true sync ? PLCout1 do { nothing } goto COM11)),
                   (@ COM7 inv (COMclk <= COMd) : 
                      (when (COMclk = COMd) sync ? CNreq0 do { nothing } goto COM8 ,
                       when true sync ? PLCout1 do { nothing } goto COM7)),
                   (@ COM8 inv (COMclk <= COMct) : 
                      (when true sync ? NCrep0 do { tt := 0 } goto COM9 ,
                       when true sync ? PLCout1 do { nothing } goto COM8 ,
                       when true sync ? NCrep1 do { tt := 0 } goto COM10)),
                   (@ COM9 inv (tt = 0/1) : 
                      (when (tt = 0/1) sync ? COMin0 do { nothing } goto COM11)),
                   (@ COM10 inv (tt = 0/1) : 
                      (when (tt = 0/1) sync ? COMin1 do { nothing } goto COM11)),
                   (@ COM11 inv (COMclk <= COMct) : 
                      (when (COMclk = COMct) local noaction do { COMclk := 0 } goto COM12 ,
                       when true sync ? PLCout1 do { nothing } goto COM11)),
                   (@ COM12 inv (COMclk <= COMd) : 
                      (when (COMclk = COMd) sync ? CNreq1 do { nothing } goto COM13 ,
                       when true sync ? PLCout1 do { nothing } goto COM12)),
                   (@ COM13 inv (COMclk <= COMct) : 
                      (when true sync ? NCrep1 do { tt := 0 } goto COM14 ,
                       when true sync ? PLCout1 do { nothing } goto COM13)),
                   (@ COM14 inv (tt = 0/1) : 
                      (when (tt = 0/1) sync ? COMin1 do { nothing } goto COM15)),
                   (@ COM15 inv (COMclk <= COMct) : 
                      (when (COMclk = COMct) local noaction do { COMclk := 0 } goto COM12 ,
                       when true sync ? PLCout1 do { nothing } goto COM15)) >
                  .

    op Net : -> Automaton .
    eq Net = < NET |
                   (@ NET1 inv true : 
                      (when true sync ! CNreq0 do { NETclk := 0 } goto NET2 ,
                       when true sync ! CNreq1 do { NETclk := 0 } goto NET5)),
                   (@ NET2 inv (NETclk <= NETd) : 
                      (when (NETclk = NETd) sync ! NRreq0 do { nothing } goto NET3)),
                   (@ NET3 inv true : 
                      (when true sync ! RNrep0 do { NETclk := 0 } goto NET4 ,
                       when true sync ! RNrep1 do { NETclk := 0 } goto NET7)),
                   (@ NET4 inv (NETclk <= NETd) : 
                      (when (NETclk = NETd) sync ! NCrep0 do { nothing } goto NET1)),
                   (@ NET5 inv (NETclk <= NETd) : 
                      (when (NETclk = NETd) sync ! NRreq1 do { nothing } goto NET6)),
                   (@ NET6 inv true : 
                      (when true sync ! RNrep1 do { NETclk := 0 } goto NET7)),
                   (@ NET7 inv (NETclk <= NETd) : 
                      (when (NETclk = NETd) sync ! NCrep1 do { nothing } goto NET1)) >
                  .

    op Rio : -> Automaton .
    eq Rio = < RIO |
                   (@ RIO1 inv true : 
                      (when true sync ? NRreq0 do { RIOclk := 0 } goto RIO2)),
                   (@ RIO2 inv (RIOclk <= RIOd) : 
                      (when (RIOclk = RIOd) sync ? SIGout0 do { zz := 0 } goto RIO3 ,
                       when true sync ? SIGin1 do { nothing } goto RIO4)),
                   (@ RIO3 inv (zz = 0/1) : 
                      (when (zz = 0/1) sync ? RNrep0 do { nothing } goto RIO1)),
                   (@ RIO4 inv (RIOclk <= RIOd) : 
                      (when (RIOclk = RIOd) sync ? SIGout0 do { zz := 0 } goto RIO5)),
                   (@ RIO5 inv (zz = 0/1) : 
                      (when (zz = 0/1) sync ? RNrep0 do { nothing } goto RIO6)),
                   (@ RIO6 inv true : 
                      (when true sync ? NRreq0 do { RIOclk := 0 } goto RIO7 ,
                       when true sync ? NRreq1 do { RIOclk := 0 } goto RIO9)),
                   (@ RIO7 inv (RIOclk <= RIOd) : 
                      (when (RIOclk = RIOd) sync ? SIGout0 do { zz := 0 } goto RIO8)),
                   (@ RIO8 inv (zz = 0/1) : 
                      (when (zz = 0/1) sync ? RNrep1 do { nothing } goto RIO6)),
                   (@ RIO9 inv (RIOclk <= RIOd) : 
                      (when (RIOclk = RIOd) sync ? SIGout1 do { zz := 0 } goto RIO10)),
                   (@ RIO10 inv (zz = 0/1) : 
                      (when (zz = 0/1) sync ? RNrep1 do { nothing } goto RIO6)) >
                  .

    op Env : -> Automaton .
    eq Env = < ENV |
                   (@ ENVinit inv true : 
                      (when true sync ! SIGout0 do { nothing } goto ENV1)),
                   (@ ENV1 inv true : 
                      (when true sync ! SIGin1 do { ENVclk := 0 } goto ENV2 ,
                       when true sync ! SIGout0 do { nothing } goto ENV1)),
                   (@ ENV2 inv (ENVclk <= SIGmrt) : 
                      (when true sync ! SIGout0 do { nothing } goto ENV2 ,
                       when true sync ! SIGout1 do { yy := 0 } goto ENV4 ,
                       when (ENVclk = SIGmrt) local noaction do { yy := 0 } goto ENV5)),
                   (@ ENV4 inv (yy = 0/1) : empty),
                   (@ ENV5 inv (yy = 0/1) : empty) >
                  .

    op init : -> State .
    eq init = ( Com, Env, Net, Plc, Rio ) : 
              <
                locs: (PLC @ PLCinit, COM @ COMinit, NET @ NET1, RIO @ RIO1, ENV @ ENVinit)
                clocks: (PLCclk : 0, COMclk : 0, NETclk : 0, RIOclk : 0, ENVclk : 0, tt : 0, yy : 0, zz : 0)
                params: (PLCmtt : rr(var(PLCmtt)), PLCct : rr(var(PLCct)), COMd : rr(var(COMd)), COMct : rr(var(COMct)), NETd : rr(var(NETd)), RIOd : rr(var(RIOd)), SIGmrt : rr(var(SIGmrt)))
                gvars: empty > || 
                ( rr(var(PLCmtt)) === 100/1 and rr(var(PLCct)) === 600/1 and rr(var(COMd)) === 25/1 and rr(var(NETd)) === 10/1 and rr(var(RIOd)) === 70/1 and rr(var(PLCct)) > 0/1 and rr(var(COMct)) > 0/1 and rr(var(SIGmrt)) > 0/1 and rr(var(PLCmtt)) > 0/1 and rr(var(RIOd)) > 0/1 and rr(var(COMd)) > 0/1 and rr(var(NETd)) > 0/1 ) : empty .

endsm

srew [1] wrap(init, EF (<replace>)) using check .



quit .

eof
