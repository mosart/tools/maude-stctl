# Benchmarks

This folder contains the scripts to generate the benchmarks of the paper for
EF-Synthesis (using only CTL). The imitator models were borrowed from the
[IMITATOR benchmarks library](https://www.imitator.fr/library.html).

## Structure of the folder

```
.
├── analysis.ipynb    # Python notebook to generate the plots (analysis.py)
├── benchmarks.sh     # Bash script to run the benchmarks
├── images            # Folder containing the plots of the benchmark results
├── models            # Folder containing the models of the benchmark
├── requirements.txt  # File containing the python requirements
└── results.csv       # CSV file with the benchmarks results
```

## Python Requirements

Create a Python environment to install the needed dependencies: 
```
python -m venv init PATH
source PATH/bin/activate
pip inistall -r requirements.txt 
```

## Run benchmarks

The script `benchmarks.sh` allows to run the benchmarks for a specific model.
Before running it, please set the variables `imitator` and `maude` with the
absolute path of IMITATOR and Maude binaries, respectively.

```
λ> ./benchmarks.sh
Usage: ./benchmarks.sh -m MODEL -t TIMEOUT
```
